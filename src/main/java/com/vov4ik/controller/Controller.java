package com.vov4ik.controller;

import com.vov4ik.model.ExecutorCreator;
import com.vov4ik.model.FibonacciSequence;
import com.vov4ik.model.PingPong;

public class Controller {

    public void showPingPong() {
        PingPong pingPong = new PingPong();
        pingPong.showPingPong();
    }

    public void showFibonacci(int sequenceSize) {
        FibonacciSequence fibonacciSequence = new FibonacciSequence(sequenceSize);
        fibonacciSequence.startFibonacci(sequenceSize);
    }

    public void showSingleExecutor(int sequenceSize) {
        ExecutorCreator executorCreator = new ExecutorCreator();
        executorCreator.singleExecutorStart(sequenceSize);
    }

    public void showOnDemandExecutor(int sequenceSize) {
        ExecutorCreator executorCreator = new ExecutorCreator();
        executorCreator.onDemandExecutorStart(sequenceSize);
    }
}
