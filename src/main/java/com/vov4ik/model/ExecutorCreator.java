package com.vov4ik.model;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorCreator {
    public void singleExecutorStart(int sequenceSize) {
        ExecutorService singleExecutorService = Executors.newSingleThreadExecutor();
        FibonacciSequence fibonacciSequence = new FibonacciSequence(sequenceSize);
        Future<Integer> futureSum = singleExecutorService.submit(fibonacciSequence::call);
        try {
            System.out.println("Sum of sequence = " + futureSum.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        singleExecutorService.shutdown();
    }

    public void onDemandExecutorStart(int sequenceSize) {
        ExecutorService onDemEx = Executors.newCachedThreadPool();
        FibonacciSequence fibonacciSequence = new FibonacciSequence(sequenceSize);
        Future<Integer> sum1 = onDemEx.submit(fibonacciSequence::call);
        try {
            System.out.println(sum1.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        onDemEx.shutdown();
    }


}

