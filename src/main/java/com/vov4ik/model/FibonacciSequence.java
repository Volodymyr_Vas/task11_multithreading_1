package com.vov4ik.model;

import java.util.Arrays;
import java.util.concurrent.Callable;

public class FibonacciSequence implements Runnable, Callable {
    private int first = 0;
    private int second = 1;
    private int sequenceSize;
    private int[] fibonacciSequence;

    public FibonacciSequence(int sequenceSize) {
        this.sequenceSize = sequenceSize;
        fibonacciSequence = buildFibonacciSequence(sequenceSize);
    }

    private boolean checkSequenceSize(int sequenceSize) {
        return sequenceSize > 0;
    }

    private int[] buildFibonacciSequence(int sequenceSize) throws IllegalArgumentException{
        if (!checkSequenceSize(sequenceSize)){
            throw new IllegalArgumentException("incorrect size");
        }else if (sequenceSize == 1) {
            fibonacciSequence  = new int[]{first};
        }else if (sequenceSize == 2) {
            fibonacciSequence  = new int[]{first, second};
        } else {
            fibonacciSequence = new int[sequenceSize];
            fillFibonacciSequence(sequenceSize);
        }
        return fibonacciSequence;
    }

    private void fillFibonacciSequence(int sequenceSize) {
        int index = 2;
        fibonacciSequence[0] = first;
        fibonacciSequence[1] = second;
        while (index < sequenceSize) {
            fibonacciSequence[index] = fibonacciSequence[index - 1]
                    + fibonacciSequence[index - 2];
            index++;
        }
    }

    public void startFibonacci(int sequenceSize) {
        Thread fibonacci1 = new Thread(new FibonacciSequence(sequenceSize));
        fibonacci1.start();
        try {
            fibonacci1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("Thread run");
        Arrays.stream(fibonacciSequence).forEach(System.out::println);
        System.out.println("run over");
    }

    @Override
    public Integer call() throws Exception {
        int sum = Arrays.stream(fibonacciSequence).sum();
        return sum;
    }
}
