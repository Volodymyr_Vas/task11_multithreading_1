package com.vov4ik.model;

public class PingPong {
    private volatile int ball = 0;
    private static final Object sync = new Object();

    public void showPingPong(){
        Thread thread1 = new Thread(() -> {
            synchronized (sync) {
                for (int i = 0; i < 20; i++) {
                    try {
                        sync.wait();
                        ball++;
                        System.out.println(Thread.currentThread().getName() + " " + ball);
                        sync.notify();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread thread2 = new Thread(() -> {
            synchronized (sync) {
                for (int i = 0; i < 20; i++) {
                    try {
                        sync.notify();
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ball++;
                    System.out.println(Thread.currentThread().getName()+ " " + ball);
                }
            }
        });
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
