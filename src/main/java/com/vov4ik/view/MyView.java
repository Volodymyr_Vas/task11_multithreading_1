
package com.vov4ik.view;


import com.vov4ik.controller.Controller;
import com.vov4ik.view.logger.MyLogger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private MyLogger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner;
    int fibonacciSequenceSize;

    public MyView() {
        logger = new MyLogger();
        controller = new Controller();
        scanner = new Scanner(System.in);
        setMenu();
        setMethodsMenu();
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Show Ping Pong");
        menu.put("2", "Show Fibonacci sequence");
        menu.put("3", "Show sum of Fibonacci sequence with single thread executor");
        menu.put("4", "Show sum of Fibonacci sequence with on demand thread executor");
        menu.put("Q", "Quit");
    }

    private void setMethodsMenu() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showPingPong);
        methodsMenu.put("2", this::showFibonacciSequence);
        methodsMenu.put("3", this::showSumWithSingleExecutor);
        methodsMenu.put("4", this::showSumWithOnDemandExecutor);
    }

    private void showPingPong() {
        controller.showPingPong();
    }

    private void showFibonacciSequence() {
        boolean correctInput = false;
        int input = -1;
        do {
            logger.printInfo("Enter a number of sequence, please:");
            try {
                input = scanner.nextInt();
                if (input < 1) {
                    throw new IllegalArgumentException("incorrect input");
                }
                correctInput = true;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        while (!correctInput);
        fibonacciSequenceSize = input;
        controller.showFibonacci(fibonacciSequenceSize);
    }

    private void showSumWithSingleExecutor() {
        controller.showSingleExecutor(fibonacciSequenceSize);
    }

    private void showSumWithOnDemandExecutor() {
        controller.showOnDemandExecutor(fibonacciSequenceSize);
    }

    public void showMenu() {
        String input;
        do {
            logger.printInfo("Menu:\n");
            for (String key : menu.keySet()) {
                logger.printInfo(key + " - " + menu.get(key));
            }
            logger.printInfo("Please, select one of points");
            input = scanner.nextLine().toUpperCase();
            if (input.equals("Q")) {
                break;
            } else try {
                methodsMenu.get(input).print();
            } catch (Exception e) {
                logger.printWarning("Your input was incorrect. \nPlease, try again");
            }
        } while (!input.equals("Q"));
    }
}


